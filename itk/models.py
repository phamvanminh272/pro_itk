from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()

class Topic_Level1(models.Model):
    name = models.TextField()

class Topic_Level2(models.Model):
    name = models.TextField()
    topic_level1_id = models.ForeignKey(Topic_Level1, on_delete=models.CASCADE)

class Mul_Choice_Question(models.Model):
    name = models.TextField()
    topic_level2_id = models.ForeignKey(Topic_Level2, on_delete=models.CASCADE)

class Mul_Choice_Answer(models.Model):
    order = models.CharField(max_length=10)
    name = models.TextField()
    is_correct = models.BooleanField()
    question_id = models.ForeignKey(Mul_Choice_Question, on_delete=models.CASCADE)