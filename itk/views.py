from django.shortcuts import render
from .models import *

def home(request):
    content = {
        'posts': Post.objects.all(),
        'topic_dbs': Topic_Level2.objects.filter(topic_level1_id=1).all(),
        'topic_prs': Topic_Level2.objects.filter(topic_level1_id=2).all(),
        'topic_nets': Topic_Level2.objects.filter(topic_level1_id=3).all(),
        'topic_oss': Topic_Level2.objects.filter(topic_level1_id=4).all()
    }
    return render(request, 'itk/home.html', content, {'title': 'Home'})
